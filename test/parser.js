/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'
const tap = require('tap')
const { Parser } = require('../src/parser.js')

tap.test('parseLine', t => {
  const cases = [
    ['', {}, {}],
    ['實事求是', {}, {}],
    ['#snipp', {}, {}],
    ["#snip 'f'", {}, {}],
    ['#pin "f" "s"', {}, {}],
    ['#snip ""s"', {}, {}],
    ['\\#snip', { line: '#snip' }, { line: '#snip' }],
    ['\\#snip "s"', { line: '#snip "s"' }, { line: '#snip "s"' }],
    ['//\\#snip \t"s"', { line: '//#snip \t"s"' }, { line: '//#snip \t"s"' }],
    ['\\\\#snip', { line: '\\#snip' }, { line: '\\#snip' }],
    ['\\\\#snip "s"', { line: '\\#snip "s"' }, { line: '\\#snip "s"' }],
    ['//\\\\#snip \t"s"', { line: '//\\#snip \t"s"' }, { line: '//\\#snip \t"s"' }],

    ['#snip', {}, { 'directive': 'end' }],
    ['    #snip', {}, { 'directive': 'end' }],
    ['#snip    ', {}, { 'directive': 'end' }],
    ['//#snip', {}, { 'directive': 'end' }],
    ['/*#snip', {}, { 'directive': 'end' }],
    ['*#snip', {}, { 'directive': 'end' }],
    ['  // #snip', {}, { 'directive': 'end' }],
    ['/* #snip', {}, { 'directive': 'end' }],
    ['  *#snip', {}, { 'directive': 'end' }],
    ['#snip\n', {}, { 'directive': 'end' }],

    [
      '#snip "s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '    #snip\t"s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '#snip  "s"    ',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '//#snip \t"s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '/*#snip \t "s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '*#snip\t "s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '\t// #snip\t \t"s"',
      { 'directive': 'begin', 'id': 's' },
      { 'directive': 'begin', 'id': 's' }
    ], [
      '/*\t#snip\t\t"foo"',
      { 'directive': 'begin', 'id': 'foo' },
      { 'directive': 'begin', 'id': 'foo' }
    ], [
      '\t*#snip "b a r"',
      { 'directive': 'begin', 'id': 'b a r' },
      { 'directive': 'begin', 'id': 'b a r' }
    ], [
      '#snip "\\"s"',
      { 'directive': 'begin', 'id': '"s' },
      { 'directive': 'begin', 'id': '"s' }
    ]
  ]

  const s = new Parser(s => 'NOP')
  for (let c of cases) {
    let [line, ifNoSnippet, ifInSnippet] = c

    s._stack = ['s']
    for (let v of [ifInSnippet, ifNoSnippet]) {
      if (v instanceof Error) {
        t.throws(() => s._parseLine(line), line)
      } else {
        if (v.line === undefined) {
          v.line = line
        }
        t.strictSame(s._parseLine(line), v, line + (s._isInSnippet() ? ' (in snippet)' : ' (outside snippet)'))
      }
      s._stack.pop()
    }
  }

  t.end()
})

tap.test('getCommonPrefix', t => {
  const cases = [
    ['', '', ''],
    ['foo', '', ''],
    ['foo', ' ', ''],
    ['foobar', 'foo', 'foo'],
    ['  // foo', '// foo', ''],
    ['// foo', '//', '//']
  ]

  const sf = new Parser()
  for (let c of cases) {
    let [a, b, common] = c
    t.equals(sf._getCommonPrefix(a, b), common, JSON.stringify(a) + ' vs ' + JSON.stringify(b))
    t.equals(sf._getCommonPrefix(b, a), common, JSON.stringify(a) + ' vs ' + JSON.stringify(b))
  }

  t.end()
})

tap.test('deindentSnippet', t => {
  const cases = [
    ['', ''],
    ['foo', 'foo'],
    ['foo   ', 'foo   '],
    ['  foo', 'foo'],
    ['\t\tfoo', 'foo'],
    [' \t  \t\tfoo', 'foo'],
    ['//foo', 'foo'],
    ['/*foo', 'foo'],
    ['*foo', 'foo'],
    ['// foo', 'foo'],
    [' /* foo', 'foo'],
    ['\t* foo', 'foo'],

    ['foo\nbar', 'foo\nbar'],
    ['  foo\n  bar\n  baz', 'foo\nbar\nbaz'],
    ['  foo\n\n  bar\n  baz', 'foo\n\nbar\nbaz'],
    ['foo\n  bar\n  baz', 'foo\n  bar\n  baz'],
    ['  foo\nbar\n  baz', '  foo\nbar\n  baz'],
    ['  foo\n  bar\nbaz', '  foo\n  bar\nbaz'],
    [' foo\n  bar\n  baz', 'foo\n bar\n baz'],
    ['  foo\n bar\n  baz', ' foo\nbar\n baz'],
    ['  foo\n  bar\n baz', ' foo\n bar\nbaz'],
    [`
        // Foo bla bla bla
        // for example:
        //
        //     a -> b
        //     b -> c
        //     ------
        //     a -> c
        //
        // etc etc`, `
Foo bla bla bla
for example:

    a -> b
    b -> c
    ------
    a -> c

etc etc`],
    [`
/**
 * @return foobar quux
 */
 function a(foo) {
    return foo * bar;
 }`, `
/**
 * @return foobar quux
 */
 function a(foo) {
    return foo * bar;
 }`]
  ]

  const sf = new Parser()
  for (let c of cases) {
    let [input, output] = c
    let lines = input.split('\n')
    const lastLine = lines.pop()
    lines = lines.map(l => l + '\n')
    lines.push(lastLine)
    t.equals(sf._deindentSnippet(lines), output, input)
  }

  t.end()
})
