/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const mkdirp = require('mkdirp')
const path = require('path')
const pump = require('pump')
const { Writable } = require('readable-stream')
const tap = require('tap')
const tmp = require('tmp')

const { Snippin, PinTx } = require('../src/index.js')

tmp.setGracefulCleanup()
const tmpDir = tmp.dirSync({ prefix: 'test-snippin', unsafeCleanup: true })
process.chdir(tmpDir.name)

// Let's set up for the examples below
const thisFile = path.parse(__filename).base
const exampleSnippin = new Snippin(__dirname)
fs.writeFileSync('test.js',
  (exampleSnippin.getSnippetsSync(thisFile))['test.js'])
fs.writeFileSync('prefixed.js',
  (exampleSnippin.getSnippetsSync(thisFile))['prefixed.js'])
fs.writeFileSync('nested.txt',
  (exampleSnippin.getSnippetsSync(thisFile))['nested.txt'])
fs.writeFileSync('escaped.txt',
  (exampleSnippin.getSnippetsSync(thisFile))['escaped.txt'])

// #snip "intro"
// **Snippin** lets you re-use snippets from text files in other contexts.
//
// Like say you're writing some documentation for your code and you want to
// show some examples of how it behaves. You figure you'll grab the examples
// from your test suite, because 1. they're already written and 2. they're
// guaranteed to work --it's so annoying to find bugs in sample code! But
// there's some boring boilerplate at the top of your test file, and there are
// some tests that go into too much detail to serve as good examples, so you
// don't want to just include the whole file in your documentation. With
// Snippin, you can identify just the specific parts you're interested in:
//
//     #snip "test.js"
//     // test.js
//     const tap = require('tap')
//
//     // #snip "example"
//     tap.equals(1 + 1, 2)
//     // #snip
//     #snip
//
// Now you can load them with Snippin and use them however you need:
const snippin = new Snippin()
tap.same(snippin.getSnippetsSync('test.js'), {
  'example': 'tap.equals(1 + 1, 2)\n'
})
// Writing snippets
// ----------------
//
// Start a snippet by writing `#snip <snippet name>` by itself on a line.
// The snippet name must be a valid JSON string, e.g.
//
//     \#snip "example snippet"
//
// End a snippet by writing just "#snip" by itself on a line:
//
//     \#snip
//
// You're allowed as much whitespace as you need before "#snip", and you can
// also have comment markers on the same line. For example,
//
//     #snip "prefixed.js"
//     // prefixed.js
//     // #snip "inside a comment"
//     //
//     // That "// " at the start of these lines
//     // won't be part of the snippet
//     // #snip
//     #snip
tap.same(snippin.getSnippetsSync('prefixed.js'), {
  'inside a comment': `
That "// " at the start of these lines
won't be part of the snippet
` })

// Specifically, here are the regex components that match whitespace and comment
// markers:
//
//  * prefix: `/{{ prefixRe }}/`
//  * suffix: `/{{ suffixRe }}/`
//
// **Snippets can be nested**. For example,
//
//     #snip "nested.txt"
//     // nested.txt
//     \#snip "outer"
//     Outer snippet
//     \#snip "inner"
//     Inner snippet
//     \#snip
//     Still outer snippet
//     \#snip
//     #snip
tap.same(snippin.getSnippetsSync('nested.txt'), {
  'outer': 'Outer snippet\nInner snippet\nStill outer snippet\n',
  'inner': 'Inner snippet\n'
})

// **You can escape "#snip" directives** by prefixing them with a "\\". And of
// course if you need a literal "\\#snip" you can write "\\\\#snip":
//
//     #snip "escaped.txt"
//     // escaped.txt
//     \#snip "snippet"
//     \\#snip "literal #snip"
//     \\#snip
//     // \\#snip "literal backslash"
//     \#snip
//     #snip
tap.same(snippin.getSnippetsSync('escaped.txt'), {
  'snippet': '#snip "literal #snip"\n#snip\n// \\#snip "literal backslash"\n'
})
// #snip

tap.test('PinTx example', t => {
  t.plan(1)
  // #snip "pin"
  const doc = `We all know that one and one make two:
    #pin "test.js" "example"`
  let output = ''
  const pinTx = new PinTx()
  pinTx.on('data', chunk => { output += chunk })
  pinTx.on('end', () => t.equals(output,
    'We all know that one and one make two:\ntap.equals(1 + 1, 2)\n'))
  pinTx.end(doc)
  // #snip
})

tap.test('parseLine', t => {
  const cases = [
    ['', {}],
    ['實事求是', {}],
    ['#snipp', {}],
    ["#snip 'f'", {}],
    ['#snip ""s"', {}],
    ['#snip', {}],
    ['#snip "s"', {}],
    ['#pin "f"', {}],
    ['#pin "f" s', {}],
    ['#pin f "s"', {}],
    ['#pin "f" "s\\"', {}],
    ['#pin "f""s"', {}],
    ['#pin "f" "s" #pin "f" "s"', {}],
    ['\\#pin "f" "s"', { line: '#pin "f" "s"' }],
    ['\t// \\#pin\t \t"f"\t\t"s"', { line: '\t// #pin\t \t"f"\t\t"s"' }],
    ['\\\\#pin "f" "s"', { line: '\\#pin "f" "s"' }],
    ['\t// \\\\#pin\t \t"f"\t\t"s"', { line: '\t// \\#pin\t \t"f"\t\t"s"' }],

    ['#pin "f" "s"', { 'file': 'f', 'snippet': 's' }],
    ['    #pin\t"f" "s"', { 'file': 'f', 'snippet': 's' }],
    ['#pin  "f"  "s"    ', { 'file': 'f', 'snippet': 's' }],
    ['//#pin \t"f"\t"s"', { 'file': 'f', 'snippet': 's' }],
    ['/*#pin \t "f" "s"', { 'file': 'f', 'snippet': 's' }],
    ['*#pin\t "f"   "s"', { 'file': 'f', 'snippet': 's' }],
    ['\t// #pin\t \t"f"\t\t"s"', { 'file': 'f', 'snippet': 's' }],
    ['/*\t#pin\t\t"foo" "bar"', { 'file': 'foo', 'snippet': 'bar' }],
    ['\t*#pin "foo"\t"b a r"', { 'file': 'foo', 'snippet': 'b a r' }],
    ['#pin "f" "\\"s"', { 'file': 'f', 'snippet': '"s' }]
  ]

  const s = new PinTx({ refDir: '.', errorLogger: s => 'NOP' })
  for (let c of cases) {
    let [line, parsed] = c
    if (parsed.line === undefined) {
      parsed.line = line
    }
    t.strictSame(s._this._parseLine(line), parsed, line)
  }

  t.end()
})

const defaultSnippin = new Snippin()
tap.equals(defaultSnippin.refDir, '.', 'default refdir')
tap.equals(defaultSnippin.warn, console.warn, 'default error logger')
const defaultPinTx = new PinTx()
tap.equals(defaultPinTx._this._warn, console.warn, 'tx default error logger')

tap.test('setSnippets and getSnippets', t => {
  const s = new Snippin()
  t.throws(() => s.getSnippets('foo'))
  s.setSnippets('foo', { 'bar': 'baz' })
  t.resolveMatch(s.getSnippets('foo'), { 'bar': 'baz' })
  t.same(s.getSnippetsSync('foo'), { 'bar': 'baz' })
  t.throws(() => s.getSnippets('bar'))
  t.throws(() => s.getSnippetsSync('bar'))
  t.end()
})

// To test the stream transform we'll set up some test files in a temporary
// directory structure. The `files` array is a list of `[fileName, content,
// snippets]` triplets. These are the files we can render and/or load for
// snippets. The `cases` array is a list of `[fileName, refDir,
// expectedRendering]` triplets. The fileName in these refers to a file in the
// `files` array. We'll pipe that file through a snippin transform (with given
// `refDir`), and check that the actual result is equal to `expectedRendering`.
// We'll also use this setup to verify that getSnippets and getSnippetsSync get
// us the same results and behave well when called for the same file.

const files = []
const cases = []

files.push(['empty', '', {}])
cases.push(['empty', '.', ''])

files.push(['no snippets', 'foo\nbar\n', {}])
cases.push(['no snippets', '.', 'foo\nbar\n'])

files.push(['no such snippet file', '#pin "does not exist" "why bother"', {}])
cases.push(['no such snippet file', '.', '#pin "does not exist" "why bother"'])

files.push(['blanks', '#pin "" ""\n#pin "" "s"\n#pin "f" ""', {}])
cases.push(['blanks', '.', '#pin "" ""\n#pin "" "s"\n#pin "f" ""'])

files.push(['no such snippet', '#pin "no snippets" "really"', {}])
cases.push(['no such snippet', '.', '#pin "no snippets" "really"'])

files.push(['one snippet', 'foo\n#snip "foo"\nSnippet contents\n#snip', { 'foo': 'Snippet contents\n' }])
files.push(['one pin', '#pin "one snippet" "foo"\nbar', {}])
files.push(['indented pin', '    #pin "one snippet" "foo"\nbar', {}])
cases.push(['one pin', '.', 'Snippet contents\nbar'])
cases.push(['indented pin', '.', 'Snippet contents\nbar'])

files.push(['two snippets', `
This is a file that contains two snippets.
#snip "first"
    This is the first snippet.
#snip
#snip "second"
// And this is the second snippet.
    #snip
#snip "first"
    Hello!
#snip
`, {
  'first': 'This is the first snippet.\nHello!\n',
  'second': 'And this is the second snippet.\n'
}])
files.push(['pin first', '#pin "two snippets" "first"', {}])
cases.push(['pin first', '.', 'This is the first snippet.\nHello!\n'])
files.push(['pin second', '#pin "two snippets" "second"', {}])
cases.push(['pin second', '.', 'And this is the second snippet.\n'])
files.push(['pin both', 'Reverse:\n#pin "two snippets" "second"\n#pin "two snippets" "first"\n', {}])
cases.push(['pin both', '.', 'Reverse:\nAnd this is the second snippet.\nThis is the first snippet.\nHello!\n'])

files.push(['ps', '#snip "s"\nParent dir snippet\n#snip', { 's': 'Parent dir snippet\n' }])
files.push(['d/s', '#snip "s"\nSubdir snippet\n#snip\n', { 's': 'Subdir snippet\n' }])

files.push(['d/pd', '#pin "s" "s"', {}])
cases.push(['d/pd', 'd', 'Subdir snippet\n'])
cases.push(['d/pd', '.', '#pin "s" "s"'])

files.push(['d/pp', '#pin "../ps" "s"', {}])
cases.push(['d/pp', 'd', 'Parent dir snippet\n'])
cases.push(['d/pp', '.', '#pin "../ps" "s"'])

files.push(['pd', '#pin "d/s" "s"', {}])
cases.push(['pd', '.', 'Subdir snippet\n'])
cases.push(['pd', 'd', '#pin "d/s" "s"'])

files.push(['pp', '#pin "ps" "s"', {}])
cases.push(['pp', '.', 'Parent dir snippet\n'])
cases.push(['pp', 'd', '#pin "ps" "s"'])

files.push(['ppd', '#pin "ps" "s"\n#pin "d/s" "s"', {}])
cases.push(['ppd', '.', 'Parent dir snippet\nSubdir snippet\n'])
cases.push(['ppd', 'd', '#pin "ps" "s"\n#pin "d/s" "s"'])

files.push(['d/ppd', '#pin "../ps" "s"\n#pin "s" "s"', {}])
cases.push(['d/ppd', 'd', 'Parent dir snippet\nSubdir snippet\n'])
cases.push(['d/ppd', '.', '#pin "../ps" "s"\n#pin "s" "s"'])

const absd = tmpDir.name
files.push(['appd', `#pin "${absd}/ps" "s"\n#pin "${absd}/d/s" "s"`, {}])
cases.push(['appd', '.', 'Parent dir snippet\nSubdir snippet\n'])
cases.push(['appd', 'd', 'Parent dir snippet\nSubdir snippet\n'])
files.push(['d/appd', `#pin "${absd}/ps" "s"\n#pin "${absd}/d/s" "s"`, {}])
cases.push(['d/appd', 'd', 'Parent dir snippet\nSubdir snippet\n'])
cases.push(['d/appd', '.', 'Parent dir snippet\nSubdir snippet\n'])

files.push(['self', '#snip "foo"\nWhoah!\n#snip\n#pin "self" "foo"', { 'foo': 'Whoah!\n' }])
cases.push(['self', '.', '#snip "foo"\nWhoah!\n#snip\nWhoah!\n'])

// Helper to exercise the refDir argument defaulting.
// It cycles through different but equivalent return values.
function getRefDir (refDir) {
  if (getRefDir.undef === undefined) {
    getRefDir.undef = false
  }
  if (refDir === '.') {
    getRefDir.undef = !getRefDir.undef
    return getRefDir.undef ? undefined : refDir
  } else {
    return refDir
  }
}

// Helper to exercise the options argument defaulting.
// We should get the same result no matter these options.
function getOptions () {
  getOptions.options = [
    undefined,
    {},
    { highWaterMark: 16 }
  ]
  let i = getOptions.options.length
  while (true) {
    i++
    if (i >= getOptions.options.length) {
      i = 0
    }
    return getOptions.options[i]
  }
}

const ENCODING = 'utf8'
const snippets = {}
for (let [fileName, content, fileSnippets] of files) {
  snippets[fileName] = fileSnippets
  mkdirp.sync(path.dirname(fileName))
  fs.writeFileSync(fileName, content, ENCODING)
}

for (let [fileName, refDir, expected] of cases) {
  let testName = `${fileName} :: ${refDir}`
  tap.test(`getSnippetsSync before getSnippets ${testName}`, t => {
    const expected = snippets[fileName]
    const s = new Snippin(undefined, s => 'NOP')
    t.plan(2)
    t.same(s.getSnippetsSync(fileName), expected)
    s.getSnippets(fileName).then(snippets => t.same(snippets, expected))
  })
  tap.test(`getSnippetsSync during getSnippets ${testName}`, t => {
    const expected = snippets[fileName]
    const s = new Snippin(undefined, s => 'NOP')
    t.plan(2)
    s.getSnippets(fileName).then(snippets => t.same(snippets, expected))
    try {
      s.getSnippetsSync(fileName)
      t.fail('should have thrown')
    } catch (e) {
      t.pass()
    }
  })
  tap.test(`getSnippetsSync after getSnippets ${testName}`, t => {
    const expected = snippets[fileName]
    const s = new Snippin(undefined, s => 'NOP')
    t.plan(2)
    s.getSnippets(fileName).then(snippets => {
      t.same(snippets, expected)
      t.same(s.getSnippetsSync(fileName), expected)
    })
  })
  tap.test(`transform ${testName}`, t => {
    let chunks = []
    let actual = null
    pump(fs.createReadStream(fileName),
      new PinTx({ refDir: getRefDir(refDir), errorLogger: s => 'NOP' }, getOptions()),
      new Writable({
        write (chunk, enc, cb) {
          chunks.push(chunk)
          cb()
        },
        final (cb) {
          actual = Buffer.concat(chunks).toString(ENCODING)
          cb()
        }
      }),
      err => {
        if (err != null) {
          t.fail(testName)
        } else {
          t.equals(actual, expected, testName)
        }
        t.end()
      })
  })
}
