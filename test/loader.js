/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'
const tap = require('tap')
const { Loader } = require('../src/loader.js')

const placateStandard = Object.prototype
placateStandard.notOwnProperty = 'placate nyc'

const testFile = (msg, file, expectedSnippets) => {
  const sf = new Loader(e => 'NOP')
  tap.test(`loadString(${msg})`, t => {
    try {
      const snippets = sf.loadString(file)
      t.strictSame(snippets, expectedSnippets, file)
      t.end()
    } catch (e) {
      if (expectedSnippets instanceof Error) {
        t.pass(file)
      } else {
        t.fail(file, { e: e, snippets: sf.snippets })
      }
      t.end()
    }
  })
}

testFile('Empty file', '', {})
testFile('Blank file', '\n\n\n', {})
testFile('Literal file', 'Foo\nBar\nFoobar Baz\n', {})

testFile('Unclosed snippet 1', '#snip "foo"', new Error())
testFile('Unclosed snippet 2', '#snip "foo"\nbar\nbaz', new Error())
testFile('Unclosed snippet 3', 'bar\n#snip "foo"\nbaz', new Error())

testFile('All snippet', `#snip "foo"
Bar
Bar
Bar
#snip
`, { 'foo': 'Bar\nBar\nBar\n' })

testFile('Prefix and snippet', `
Bar Bar Bar
#snip "foo"
Foobar
Barbaz
#snip`, { 'foo': 'Foobar\nBarbaz\n' })

testFile('Snippet and suffix', `#snip "foo"
            Praesent efficitur tempor nibh,
            sed lacinia neque volutpat eget.
            Donec suscipit massa quis dictum suscipit.
            #snip
            Aliquam varius ac mauris at dictum.
            Donec aliquet consequat massa,
            facilisis fermentum ante faucibus eget.
            Interdum et malesuada fames ac ante ipsum primis in faucibus.`,
{ 'foo':
`Praesent efficitur tempor nibh,
sed lacinia neque volutpat eget.
Donec suscipit massa quis dictum suscipit.
` })

testFile('Two snippets', `
For example,

    #snip "Ex 1"
    1 + 1 = 2
    #snip

or

    #snip "Ex 2"
    1 + 2 = 3
    #snip
`, {
  'Ex 1': '1 + 1 = 2\n',
  'Ex 2': '1 + 2 = 3\n'
})

testFile('Multipart snippet', `
For example,

    #snip "Ex 1"
    1 + 1 = 2
    #snip

or

    #snip "Ex 1"
    1 + 2 = 3
    #snip

or even

        #snip "Ex 1"
        2 + 3 = 5
        #snip
`, {
  'Ex 1': '1 + 1 = 2\n1 + 2 = 3\n2 + 3 = 5\n'
})

testFile('Snippet with name of common object property', `
#snip "toString"
trololo
#snip
`, {
  'toString': 'trololo\n'
})

testFile('Multi-name snippet', `
#snip "a"
#snip "b"
a/b
#snip
#snip
`, {
  'a': 'a/b\n',
  'b': 'a/b\n'
})

testFile('Nested snippets', `
#snip "a"
A
#snip "b"
B
#snip "c"
#snip "d"
C/D
#snip
C
#snip
#snip
A
#snip
`, {
  'a': 'A\nB\nC/D\nC\nA\n',
  'b': 'B\nC/D\nC\n',
  'c': 'C/D\nC\n',
  'd': 'C/D\n'
})

const defaultLoader = new Loader()
tap.equals(defaultLoader.warn, console.warn, 'default error logger')

// #snip "loadString example"
const loader = new Loader()
const doc = `
#snip "intro"
Hello,
#snip

have a nice day.

#snip "conclusion"
Good bye!
#snip
`
tap.same(loader.loadString(doc), {
  intro: 'Hello,\n',
  conclusion: 'Good bye!\n'
})
// #snip
