/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const assert = require('assert')
const regexps = require('./regexps.js')

class Parser {
  constructor (errorLogger = console.warn) {
    this._snippets = {}
    this._beginRe = new RegExp(regexps.prefix + '\\\\{0,2}#snip\\s+(".+")' + regexps.suffix)
    this._endRe = new RegExp(regexps.prefix + '\\\\{0,2}#snip' + regexps.suffix)
    this._escRe = new RegExp(regexps.prefix + '\\\\{1,2}')

    this.warn = errorLogger
    this._stack = []
  }

  addLine (line) {
    const parsed = this._parseLine(line)
    switch (parsed.directive) {
      case 'begin':
        if (!(this._snippets.hasOwnProperty(parsed.id))) {
          this._snippets[parsed.id] = []
        }
        this._snippets[parsed.id].push([])
        this._stack.push(parsed.id)
        break
      case 'end':
        this._stack.pop()
        break
      default:
        if (this._isInSnippet()) {
          this._stack.forEach(s => {
            const i = this._snippets[s].length - 1
            this._snippets[s][i].push(parsed.line)
          })
        }
    }
  }

  getSnippets () {
    if (this._isInSnippet()) {
      throw new Error(`Missing closing tag for snippet "${this._stack[0]}"`)
    }
    this._normalizeSnippets()

    return this._snippets
  }

  _parseLine (line) {
    // First, test for escaped directives
    if (this._beginRe.test(line) || this._endRe.test(line)) {
      let e = this._escRe.exec(line)
      if (e !== null) {
        assert.ok(e[0].length > 0)
        return {
          line: line.substring(0, e[0].length - 1) + line.substring(e[0].length)
        }
      }
    }

    // OK check for actual directives
    const ret = { line: line }
    if (this._isInSnippet() && this._endRe.test(line)) {
      ret.directive = 'end'
    } else {
      let m = this._beginRe.exec(line)
      if (m !== null) {
        try {
          ret.id = JSON.parse(m[1])
          ret.directive = 'begin'
        } catch (e) {
          this.warn('This line looks like the start of a snippet,' +
            " but the snippet name isn't valid JSON, so it will" +
            ' be treated as normal text: ' + line)
        }
      }
    }

    return ret
  }

  _normalizeSnippets () {
    for (let name in this._snippets) {
      if (this._snippets.hasOwnProperty(name)) {
        this._snippets[name] = this._snippets[name]
          .map(s => this._deindentSnippet(s))
          .join('')
      }
    }
  }

  _deindentSnippet (snippet) {
    // Every line has two parts: prefix and content. If content is blank,
    // then I can ignore the line (it's just vertical whitespace). Otherwise,
    // its prefix competes for shortest. Once I find the shortest prefix,
    // I remove it from all lines.
    let deindented = []

    let shortestPrefix = null

    // Find the shortest prefix
    for (let line of snippet) {
      let m = line.match(new RegExp(regexps.prefix))
      if (m === null || m[0].length === line.length) {
        // Just whitespace; this doesn't compete for shortest prefix.
        continue
      }
      if (shortestPrefix === null) {
        shortestPrefix = m[0]
      } else {
        shortestPrefix = this._getCommonPrefix(shortestPrefix, m[0])
      }
    }

    if (shortestPrefix === null) {
      return snippet.join('')
    }

    for (let line of snippet) {
      // Because whitespace lines didn't participate in the shortest
      // prefix search, we have no guarantee that all lines begin with
      // said shortest prefix. We have to look for a common prefix again
      // line-by-line.
      let prefixToRemove = this._getCommonPrefix(shortestPrefix, line)
      if (prefixToRemove.length > 0) {
        line = line.replace(prefixToRemove, '')
      }
      deindented.push(line)
    }

    return deindented.join('')
  }

  _getCommonPrefix (shorter, longer) {
    if (longer.length < shorter.length) {
      const tmp = shorter
      shorter = longer
      longer = tmp
    }

    while (shorter.length > 0 && longer.slice(0, shorter.length) !== shorter) {
      shorter = shorter.slice(0, -1)
    }

    return shorter
  }

  _isInSnippet () {
    return this._stack.length > 0
  }
}

module.exports.Parser = Parser
