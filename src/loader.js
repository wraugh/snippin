/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const { SplitReader, splitStr } = require('split-anything')

const { Parser } = require('./parser.js')
const regexps = require('./regexps.js')

class Loader {
  /* #snip "constructor"
   * **new Loader([errorLogger])**
   *
   * > - `errorLogger` *Function* Function to use to log errors; it will be
   * >                            called with a single argument: an error message
   * >                            string. **Default:** `console.warn`.
   * #snip
   */
  constructor (errorLogger = console.warn) {
    this.warn = errorLogger

    this.lineBreakRegexp = new RegExp(regexps.lineBreak)
  }

  /* #snip "loadFile"
   * **loadFile(fileName)**
   *
   * >  - `fileName` *string*  The path to the file from which to extract snippets.
   * >  - Returns: *Promise* Resolves to the snippets found in `fileName`. Keys
   * >                       are the snippet names, values are their contents.
   * >
   * > Gets snippets from the given file.
   * #snip
   */
  loadFile (fileName) {
    const reader = new SplitReader(fileName, 'utf8', this.lineBreakRegexp)
    const parser = new Parser(this.warn)
    const readTilEnd = () => reader.read().then(line => {
      if (line === null) {
        return parser.getSnippets()
      }
      parser.addLine(line)

      return readTilEnd()
    })

    return readTilEnd()
  }

  /* #snip "loadFileSync"
   * **loadFileSync(fileName)**
   *
   * >  - `fileName` *string*  The path to the file from which to extract snippets.
   * >  - Returns: *Object* The snippets found in `fileName`. Keys are the snippet
   * >                      names, values are their contents.
   * >
   * > Gets snippets from the given file synchronously.
   * #snip
   */
  loadFileSync (fileName) {
    const reader = new SplitReader(fileName, 'utf8', this.lineBreakRegexp)
    const parser = new Parser(this.warn)
    let line
    while ((line = reader.readSync(), line !== null)) {
      parser.addLine(line)
    }

    return parser.getSnippets()
  }

  /* #snip "loadString"
   * **loadString(str)**
   *
   * >  - `str` *string* The text from which to extract snippets.
   * >  - Returns: *Object* The snippets found in `str`. Keys are the snippet
   * >                      names, values are their contents.
   * >
   * > Gets snippets from the given string.
   * #snip
   */
  loadString (str) {
    const parser = new Parser(this.warn)
    for (let line of splitStr(str, this.lineBreakRegexp)) {
      parser.addLine(line)
    }

    return parser.getSnippets()
  }
}

module.exports.Loader = Loader
