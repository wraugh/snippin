/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const assert = require('assert')
const path = require('path')
const pumpify = require('pumpify')
const { Transform } = require('readable-stream')
const { SplitTransform } = require('split-anything')

const { Loader } = require('./loader.js')

const regexps = require('./regexps.js')

class Snippin {
  /* #snip "Snippin constructor"
   * **new Snippin([options, [streamOptions]])**
   *
   * >  - `refDir`      *string*   The directory relative to which snippets are loaded.
   * >    **Default:** `'.'`.
   * >  - `errorLogger` *Function* Function to use to log warnings; it will be called
   * >    with a single argument: an error message string.
   * >    **Default:** `console.warn`.
   * #snip
   */
  constructor (refDir = '.', errorLogger = console.warn) {
    this.snippets = {}
    this.snippetPromises = {}
    this.refDir = refDir
    this.warn = errorLogger

    this.loader = new Loader(this.warn)
  }

  /* #snip "setSnippets"
   *
   * **snippin.setSnippets(file, snippets)**
   *
   * >  - `file`     *string* The name of the file for which to set snippets. This
   * >    can be a path to a real file, or just a name.
   * >  - `snippets` *Object* The snippets to set; properties are snippet names,
   * >    and their value is the snippet contents.
   * >
   * > Snippets set this way take precedence over snippets loaded from files. This
   * > function operates on the entire set of snippets for a file; it will replace
   * > the set of snippets for the given file if it's already been loaded, or
   * > prevent it from being loaded if it hasn't yet.
   *
   * #snip
   */
  setSnippets (file, snippets) {
    this.snippets[file] = snippets
    this.snippetPromises[file] = new Promise(resolve => resolve(this.snippets[file]))
  }

  /* #snip "getSnippets"
   * **snippin.getSnippets(file)**
   *
   * >  - `file`  *string*  The name of the file for which to get snippets.
   * >  - Returns *Promise* Resolves to an object containing the snippets found in
   * >    `file`; keys are the snippet names, values are their
   * >    contents.
   * >
   * > Gets snippets from the given file. Throws if `file` doesn't exist.
   * #snip
   */
  getSnippets (fileName) {
    const f = this._getFileKey(fileName)
    if (this.snippetPromises[f] === undefined) {
      this.snippetPromises[f] = this.loader.loadFile(f)
      this.snippetPromises[f].then(snippets => { this.snippets[f] = snippets })
    }

    return this.snippetPromises[f]
  }

  /* #snip "getSnippetsSync"
   * **snippin.getSnippetsSync(file)**
   *
   * >  - `file`  *string*  The name of the file for which to get snippets.
   * >  - Returns *Object* The snippets found in `file`; keys are the snippet
   * >    names, values are their contents.
   * >
   * > Gets snippets from the given file synchronously.
   * > Throws Error if `file` doesn't exist.
   * #snip
   */
  getSnippetsSync (fileName) {
    const f = this._getFileKey(fileName)
    if (this.snippets[f] === undefined) {
      if (this.snippetPromises[f] !== undefined) {
        throw new Error(`The snippets for ${fileName} are already being loaded asynchronously`)
      }
      this.snippets[f] = this.loader.loadFileSync(f)
      this.snippetPromises[f] = new Promise((resolve, reject) =>
        resolve(this.snippets[f]))
    }

    return this.snippets[f]
  }

  _getFileKey (fileName) {
    return path.isAbsolute(fileName)
      ? fileName : path.normalize(this.refDir + '/' + fileName)
  }
}

class PinTx extends Transform {
  /* #snip "PinTx constructor"
   * **new PinTx([options, [streamOptions]])**
   *
   * >  - `options` *Object*
   * >    - `refDir`      *string*   The directory relative to which snippets are loaded.
   * >      **Default:** `'.'`.
   * >    - `errorLogger` *Function* Function to use to log warnings; it will be called
   * >      with a single argument: an error message string.
   * >      **Default:** `console.warn`.
   * >  - `streamOptions` *Object* Passed as-is to [stream.Transform](
   * >    https://nodejs.org/api/stream.html#stream_new_stream_transform_options)'s
   * >    constructor.
   * >    **Default:** `{}`.
   * >
   * > This transform replaces each `#pin` directive with the snippet it references.
   * >
   * > If a `#pin` directive is malformed or references a snippet which can't be
   * > loaded, a warning is logged, and the directive is output as-is.
   * #snip
   */
  constructor (options = {}, streamOptions = {}) {
    super(streamOptions)

    this._snippin = new Snippin(options.refDir, options.errorLogger)
    this._pinRe = new RegExp(regexps.prefix + '\\\\{0,2}#pin\\s+(".+")\\s+(".+")' + regexps.suffix)
    this._escRe = new RegExp(regexps.prefix + '\\\\{1,2}')
    this._warn = options.errorLogger || console.warn

    const tx = pumpify(
      new SplitTransform(new RegExp(regexps.lineBreak), false, streamOptions),
      this
    )

    tx._this = this

    return tx
  }

  _transform (chunk, encoding, callback) {
    this._renderLine(chunk).then(rendered => callback(null, rendered))
  }

  _renderLine (line) {
    return new Promise(resolve => {
      const p = this._parseLine(line)
      if (p.file && p.snippet) {
        try {
          this._snippin.getSnippets(p.file).then(snippets => {
            if (snippets[p.snippet] != null) {
              resolve(snippets[p.snippet])
            } else {
              this._warn('No snippet named ' + JSON.stringify(p.snippet) +
                ' in file ' + JSON.stringify(p.file))
              resolve(p.line)
            }
          })
        } catch (e) {
          this._warn("Couldn't load a file named " + JSON.stringify(p.file) +
            ': ' + e)
          resolve(p.line)
        }
      } else {
        resolve(p.line)
      }
    })
  }

  _parseLine (line) {
    const m = this._pinRe.exec(line)
    if (m !== null) {
      // but is it escaped?
      const e = this._escRe.exec(line)
      if (e !== null) {
        assert.ok(e[0].length > 0)
        return {
          line: line.substring(0, e[0].length - 1) + line.substring(e[0].length)
        }
      }

      // ok it's not escaped
      try {
        return {
          line: line,
          file: JSON.parse(m[1]),
          snippet: JSON.parse(m[2])
        }
      } catch (e) {
        this._warn('This line looks like the start of a pin,' +
          " but the pin file and/or id aren't valid JSON, so it will" +
          ' be treated as normal text: ' + line)
      }
    }

    return { line: line }
  }
}

module.exports.Snippin = Snippin
module.exports.PinTx = PinTx
