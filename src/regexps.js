/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

// #snip "prefix suffix regexes"
const prefix = '^\\s*(?://|\\*|/\\*)?\\s*'
const suffix = '\\s*$'
// #snip

module.exports.prefix = prefix
module.exports.suffix = suffix

// from https://github.com/jahewson/node-byline/blob/master/lib/byline.js#L103
module.exports.lineBreak = '(\r\n|[\n\v\f\r\x85\u2028\u2029])'
