#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const { code2doc } = require('marcco')
const { rewriteRequires } = require('public-requires')
const ST = require('stream-template')

const { Snippin } = require('./src')
const bin = require('./bin/snippin')
const regexps = require('./src/regexps')

// presentation helpers
const tplVars = {
  prefixRe: regexps.prefix,
  suffixRe: regexps.suffix
}
const pencil = str => str.replace(/\{\{\s*([^{}\s]+)\s*\}\}/g, (m, v) => tplVars[v] || m)
const rr = src => rewriteRequires('snippin', __dirname, __dirname, src)
const js2doc = src => code2doc(rr(pencil(src)), { codePrefix: '~~~javascript' })

const s = new Snippin()
const pin = (file, snippet) => (s.getSnippetsSync(file))[snippet]
const pinDoc = (file, snippet) => js2doc(pin(file, snippet))

process.chdir(__dirname)
const out = fs.createWriteStream('./README.md')

ST`${pinDoc('test/snippin.js', 'intro')}

API
---

${pin('src/index.js', 'Snippin constructor')}

${pin('src/index.js', 'getSnippets')}

${pin('src/index.js', 'getSnippetsSync')}

${pin('src/index.js', 'setSnippets')}

Including snippets
------------------

Once you've loaded your snippets, you can include them into your text by using
any of the popular templating libraries, or template literals, or whatever.

Snippin also ships with a stream transform that can process "#pin" directives
and render them as the named snippet. Include a snippet by writing \`#pin file
snippet-name\` by itself on a line. Both the file and snippet name must be
valid JSON strings.

Continuing the first example above, we can do

${pinDoc('test/snippin.js', 'pin')}

${pin('src/index.js', 'PinTx constructor')}

Command line
------------

There's a command line interface to the stream transform.

~~~
${bin.usage}
~~~

Contributing
------------

This project is deliberately left imperfect to encourage you to participate in
its development. If you make a Pull Request that

 - explains and solves a problem,
 - follows [standard style](https://standardjs.com/), and
 - maintains 100% test coverage

it _will_ be merged: this project follows the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

To make sure your commits follow the style guide and pass all tests, you can add

    ./.pre-commit

to your git pre-commit hook.
`.pipe(out)
