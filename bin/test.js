/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'
const { spawnSync } = require('child_process')
const fs = require('fs')
const path = require('path')
const tap = require('tap')
const tmp = require('tmp')

const snippinBin = require('./snippin')

tmp.setGracefulCleanup()

const dir = path.join(__dirname, '../test/files')
const infile = path.join(dir, 'input.txt')

const equivalentCmds = [
  { args: [], cwd: dir, stdin: true },
  { args: ['-'], cwd: dir, stdin: true },
  { args: [infile] },
  { args: ['-o', '{{ outfile }}'], cwd: dir, stdin: true },
  { args: ['--outfile', '{{ outfile }}', '-'], cwd: dir, stdin: true },
  { args: ['-o', '{{ outfile }}', infile] },
  { args: ['--outfile', '{{ outfile }}', '-'], cwd: dir, stdin: true },
  { args: ['-o', '{{ outfile }}', infile] },
  { args: ['-d', dir], stdin: true },
  { args: ['--refdir', dir, '-'], stdin: true },
  { args: ['-d', dir, infile] },
  { args: ['--refdir', dir, '--outfile', '{{ outfile }}'], stdin: true },
  { args: ['-', '-d', dir, '-o', '{{ outfile }}'], stdin: true },
  { args: [infile, '--refdir', dir, '--outfile', '{{ outfile }}'] }
]

const expected = fs.readFileSync(path.join(dir, 'expected.txt'))

for (let cmd of equivalentCmds) {
  let [status, output] = runCmd(cmd)
  tap.equals(status, 0, 'successful exit for ' + JSON.stringify(cmd))
  tap.equals(output.toString(), expected.toString(), 'output of ' + JSON.stringify(cmd))
}

const failCmds = [
  ['/no/such/snippin/file'],
  ['-d', '/no/such/snippin/dir'],
  ['-o', '/no/such/snippin/file', infile],
  [infile, 'extra', 'junk', 'args'],
  ['--nosuchflag', infile]
]

for (let args of failCmds) {
  let [status] = runCmd({ args: args })
  tap.notEquals(status, 0, 'unsuccessful exit  for ' + JSON.stringify(args))
}

let status, output;
[status, output] = runCmd({ args: [path.join(dir, 'empty.txt')] })
tap.equals(status, 0, "It's OK to render an empty file")
tap.equals(output.toString(), '', 'Empty input produces empty output');

[status, output] = runCmd({ args: ['-h'] })
tap.equals(status, 0, "It's OK to ask for help")
tap.equals(output.toString(), snippinBin.usage + '\n', 'help shows usage message')

function runCmd (cmd) {
  const options = {
    timeout: 5000
  }
  if (cmd.cwd) {
    options.cwd = cmd.cwd
  }
  if (cmd.stdin) {
    options.input = fs.readFileSync(infile)
  }

  const [args, outputFile] = parseArgs(cmd.args)
  args.unshift(path.normalize(path.join(__dirname, '../bin/snippin')))

  const child = spawnSync('node', args, options)

  return [
    child.status,
    outputFile ? fs.readFileSync(outputFile) : child.stdout
  ]
};

function parseArgs (args) {
  let tmpFile = null
  for (let i = 0; i < args.length; i++) {
    if (args[i] === '{{ outfile }}') {
      tmpFile = tmp.fileSync().name
      args[i] = tmpFile
    }
  }

  return [args, tmpFile]
};
