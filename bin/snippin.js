#!/usr/bin/env node

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const fs = require('fs')
const getopts = require('getopts')
const path = require('path')
const pump = require('pump')

const { PinTx } = require('../src')

const ERR_MISC = 1
const ERR_USAGE = 2

// The point of this script is to call `main`. The rest of it is just about
// grabbing the arguments to `main` from the environment.
// #snip "main"
const main = (src, dest, refDir) => {
  pump(src, new PinTx({ refDir: refDir }), dest, err => {
    if (err != null) {
      console.warn(err.toString())
      process.exit(ERR_MISC)
    }
  })
}
// #snip

const usage =
`Usage: snippin [-d REFDIR] [-o OUTFILE] [--] [INFILE]

Print standard input (or INFILE) to standard output (or OUTFILE)
with #pins expanded relative to the current working directory
(or INFILE's directory, or REFDIR).

Options:

    -d, --refdir=DIRECTORY  load snippets relative to DIRECTORY
    -o, --outfile=FILE      write output to FILE
    -h, --help              show this usage message`

const fatalUsage = () => {
  console.warn(usage)
  process.exit(ERR_USAGE)
}

const opts = getopts(process.argv.slice(2), {
  alias: {
    'd': 'refdir',
    'o': 'outfile',
    'h': 'help'
  },
  string: ['d', 'o'],
  boolean: ['h'],
  unknown: opt => {
    console.warn(`Unknown option ${JSON.stringify(opt)}`)
    fatalUsage()
  }
})
if (opts._.length > 1) {
  fatalUsage()
}
if (opts.h) {
  console.log(usage)
  process.exit(0)
}

const fromStdin = opts._.length === 0 || opts._[0] === '-'
const infile = fromStdin ? null : opts._[0]
const toStdout = !opts.o || opts.o === '-'

const src = fromStdin ? process.stdin : fs.createReadStream(infile)
const dest = toStdout ? process.stdout : fs.createWriteStream(opts.o)
const refDir = opts.d ? opts.d : (fromStdin ? process.cwd() : path.dirname(infile))

if (infile) {
  try {
    fs.accessSync(infile, fs.constants.R_OK)
  } catch (e) {
    console.warn(`Error: can't read file ${infile}`)
    process.exit(ERR_MISC)
  }
}

try {
  fs.accessSync(refDir, fs.constants.R_OK | fs.constants.X_OK)
} catch (e) {
  console.warn(`Error: can't access directory ${refDir}`)
  process.exit(ERR_MISC)
}

if (require.main === module) {
  main(src, dest, refDir)
}

module.exports.usage = usage
